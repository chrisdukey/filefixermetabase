from kivy.app import App
from kivy.uix.widget import Widget


class FixFile(Widget):
    pass


class FixFileApp(App):
    def build(self):
        return FixFile()


if __name__ == '__main__':
    FixFileApp().run()

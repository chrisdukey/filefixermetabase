from cx_Freeze import setup, Executable

base = None    

executables = [Executable("FileFixer.py", base=base)]

packages = ["idna","sys","pandas","pyforms.controls","pyforms.basewidget"]
options = {
    'build_exe': {    
        'packages':packages,
    },    
}

setup(
    name = "Metawear File Combiner",
    options = options,
    version = "1",
    description = 'Combines Accel and Gyro files',
    executables = executables
)

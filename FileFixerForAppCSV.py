from pyforms.basewidget import BaseWidget
from pyforms.controls   import ControlFile
from pyforms.controls   import ControlDir
from pyforms.controls   import ControlText
from pyforms.controls   import ControlSlider
from pyforms.controls   import ControlPlayer
from pyforms.controls   import ControlButton
import sys
import pandas as pd
# import ntpath

class FileFixer(BaseWidget):

    def tweakDataFrame(this, frame):
      frame.index.name = 'Record'
      #frame.drop(['elapsed (s)'], axis=1, inplace=True) # Remove this column
     # frame.insert(0, 'Type', 1)
      #frame.insert(3, 'Date', 0)

     # frame['Date'] = pd.to_datetime(frame['Time (-00:00)'], format='%Y-%m-%dT%H:%M:%S.%f').dt.strftime('%S:%M:%H')
      #frame['time (-00:00)'] = pd.to_datetime(frame['Time (-00:00)'], format='%Y-%m-%dT%H:%M:%S.%f').dt.strftime('%d/%m/%Y')
# ios     frame['Date'] = pd.to_datetime(frame['time (-00:00)'], format='%Y-%m-%dT%H.%M.%S.%f').dt.strftime('%S:%M:%H')
# ios     frame['time (-00:00)'] = pd.to_datetime(frame['time (-00:00)'], format='%Y-%m-%dT%H.%M.%S.%f').dt.strftime('%d/%m/%Y')

      return frame

    def __init__(self, *args, **kwargs):
        super().__init__('File combiner')

        #Definition of the forms fields

        self._accelfile     = ControlFile('Accelerometer')
        self._gyrofile     = ControlFile('Gyroscope')
        self._outputfile    = ControlDir('Output file location')
        self._runbutton     = ControlButton('Combine Files')

        #Define the event that will be called when the run button is processed
        self._runbutton.value       = self.__runEvent

        #Define the organization of the Form Controls
        self._formset = [
            ('_accelfile'),
            ('_gyrofile'),
            ( '_runbutton')
        ]


    def __runEvent(self):

        # head, tail = ntpath.split(self._gyrofile.value)
        # print(head)
        # print(tail)

        outputfile = self._gyrofile.value.replace("Gyro", "Combined")

        df = pd.read_csv(self._gyrofile.value)
       # df = self.tweakDataFrame(df)
        df.drop(['Timestamp'], axis=1, inplace=True)
        df.index.name = 'Record'

      #  df = df[['Record','Date','Time','TimestampRaw','AccelX','AccelY','AccelZ']]

        df2 = pd.read_csv(self._accelfile.value)
       # df2 = self.tweakDataFrame(df2)
        df2.drop(['Timestamp'], axis=1, inplace=True)
        df2.index.name = 'Record'
       # df2 = df2[['Record','Date','Time','TimestampRaw','GyroX','GyroY','GyroZ']]


        df2.drop(['Time'], axis=1, inplace=True)
        df2.drop(['Date'], axis=1, inplace=True)
        df2.drop(['Record'], axis=1, inplace=True)


        df_merged = pd.merge_asof(df.sort_values('TimestampRaw'), df2.sort_values('TimestampRaw'), on='TimestampRaw')
        df_merged.columns = ['Record', 'Time', 'Date', 'Timestamp', 'GyroX', 'GyroY', 'GyroZ', 'AccelX', 'AccelY', 'AccelZ']
        df_merged.drop(['Record'], axis=1, inplace=True) # Remove this column

        df_merged.index.name = 'Record'

        df_merged.to_csv(outputfile)

        self.success('File combined successfully to ' + outputfile, title='Done!')


if __name__ == '__main__':

    from pyforms import start_app
    start_app(FileFixer, geometry=(200, 200, 800, 200))

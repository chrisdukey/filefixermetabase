# FileFixer python apps to combine Metawear accel, gyro and mag files

Install python if not already installed and then run the following to install dependencies:

```sh
$ pip install pyforms
$ pip install pandas
```

# FileFixerForMetabase.py
Use to combine accelerometer and gyroscope data collected using Metabase. 

# FileFixerForMetabaseWithMag.py
Use to combine accelerometer, gyroscope and magnetometer data collected using Metabase. 

# FileFixerForAppCSV.py
Use to combine accelerometer and gyroscope data collected using the Edge app and stored locally on the phone/watch

# FileFixerForAPI.py
Use to combine accelerometer and gyroscope data collected using the Edge app that has been pushed to the server and stored in S3


License
----

MIT

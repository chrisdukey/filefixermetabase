import sys
import pandas as pd
import numpy as np
from scipy.signal import butter, lfilter, freqz

df = pd.read_csv(sys.argv[1]) #Reading the Gyro
df2 = pd.read_csv(sys.argv[2]) #Reading the Accel

def tweakDataFrame(frame):
  frame.index.name = 'Record'
  frame.drop(['elapsed (s)'], axis=1, inplace=True) # Remove this column
  frame.insert(0, 'Type', 1)
  frame.insert(3, 'Date', 0)

  frame['Date'] = pd.to_datetime(frame['time (-00:00)'], format='%Y-%m-%dT%H:%M:%S.%f').dt.strftime('%S:%M:%H')
  frame['time (-00:00)'] = pd.to_datetime(frame['time (-00:00)'], format='%Y-%m-%dT%H:%M:%S.%f').dt.strftime('%d/%m/%Y')

  return frame

df = tweakDataFrame(df)
df = df[['Type','time (-00:00)','Date','epoch (ms)','x-axis (deg/s)','y-axis (deg/s)','z-axis (deg/s)']]

def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    return b, a

def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = lfilter(b, a, data)
    return y


# Filter requirements.
order = 3
fs = 100.0       # sample rate, Hz
# Kyle cutoff = 20 order = 5  # desired cutoff frequency of the filter, Hz
cutoff = 20  # desired cutoff frequency of the filter, Hz

df['x-axis (deg/s)'] = df['x-axis (deg/s)'].apply(lambda x: x*2**16).round(0).astype(int)
df['y-axis (deg/s)'] = df['y-axis (deg/s)'].apply(lambda x: x*2**16).round(0).astype(int)
df['z-axis (deg/s)'] = df['z-axis (deg/s)'].apply(lambda x: x*2**16).round(0).astype(int)

df2 = tweakDataFrame(df2)
df2 = df2[['Type','time (-00:00)','Date','epoch (ms)','x-axis (g)','y-axis (g)','z-axis (g)']]

df2['x-axis (g)'] = butter_lowpass_filter(df2['x-axis (g)'],cutoff, fs, order)
df2['y-axis (g)'] = butter_lowpass_filter(df2['y-axis (g)'],cutoff, fs, order)
df2['z-axis (g)'] = butter_lowpass_filter(df2['z-axis (g)'],cutoff, fs, order)

df2['x-axis (g)'] = df2['x-axis (g)'].apply(lambda x: x*2**16).round(0).astype(int)
df2['y-axis (g)'] = df2['y-axis (g)'].apply(lambda x: x*2**16).round(0).astype(int)
df2['z-axis (g)'] = df2['z-axis (g)'].apply(lambda x: x*2**16).round(0).astype(int)

df2.drop(['Type'], axis=1, inplace=True)
df2.drop(['Date'], axis=1, inplace=True)
df2.drop(['time (-00:00)'], axis=1, inplace=True)
print df.head(5)



df_merged = pd.merge_asof(df, df2, on='epoch (ms)')

df_merged.columns = ['Type', 'Time', 'Date', 'Timestamp', 'GyroX', 'GyroY', 'GyroZ', 'AccelX', 'AccelY', 'AccelZ']

df_merged.index.name = 'Record'

print df_merged.head(5)


#df_merged.to_csv("../CD-toilet-walk_MetaWear_2018-11-15T14.23.20.064_C9990D07A353_combined.csv")
df_merged.iloc[::2].to_csv(sys.argv[3])

#Record, Type, Time, Date, Timestamp, GyroX, GyroY, GyroZ, AccelX, AccelY, AccelZ

from pyforms.basewidget import BaseWidget
from pyforms.controls   import ControlFile
from pyforms.controls   import ControlDir
from pyforms.controls   import ControlText
from pyforms.controls   import ControlSlider
from pyforms.controls   import ControlPlayer
from pyforms.controls   import ControlButton
import sys
import pandas as pd
# import ntpath

class FileFixer(BaseWidget):

    def tweakDataFrame(this, frame):
      frame.index.name = 'Record'
      frame.drop(['elapsed (s)'], axis=1, inplace=True) # Remove this column
      frame.insert(0, 'Type', 1)
      frame.insert(3, 'Date', 0)

      frame['Date'] = pd.to_datetime(frame['time (-00:00)'], format='%Y-%m-%dT%H:%M:%S.%f').dt.strftime('%S:%M:%H')
      frame['time (-00:00)'] = pd.to_datetime(frame['time (-00:00)'], format='%Y-%m-%dT%H:%M:%S.%f').dt.strftime('%d/%m/%Y')
# ios     frame['Date'] = pd.to_datetime(frame['time (-00:00)'], format='%Y-%m-%dT%H.%M.%S.%f').dt.strftime('%S:%M:%H')
# ios     frame['time (-00:00)'] = pd.to_datetime(frame['time (-00:00)'], format='%Y-%m-%dT%H.%M.%S.%f').dt.strftime('%d/%m/%Y')

      return frame

    def __init__(self, *args, **kwargs):
        super().__init__('File combiner')

        #Definition of the forms fields

        self._accelfile     = ControlFile('Accelerometer')
        self._gyrofile     = ControlFile('Gyroscope')
        self._magfile     = ControlFile('Magnetometer')
        self._outputfile    = ControlDir('Output file location')
        self._clearbutton     = ControlButton('Clear Paths')
        self._runbutton     = ControlButton('Combine Files')

        #Define the event that will be called when the run button is processed
        self._runbutton.value       = self.__combineEvent
        self._clearbutton.value       = self.__clearEvent

        #Define the organization of the Form Controls
        self._formset = [
            ('_accelfile'),
            ('_gyrofile'),
            ('_magfile'),
            ('_clearbutton'),
            ( '_runbutton')
        ]


    def __clearEvent(self):
        self._accelfile.value = ''
        self._gyrofile.value = ''
        self._magfile.value = ''


    def __combineEvent(self):

        # head, tail = ntpath.split(self._gyrofile.value)
        # print(head)
        # print(tail)

        outputfile = self._gyrofile.value.replace("Gyroscope", "Combined")

        df = pd.read_csv(self._gyrofile.value)
        df = self.tweakDataFrame(df)
        df = df[['Type','time (-00:00)','Date','epoch (ms)','x-axis (deg/s)','y-axis (deg/s)','z-axis (deg/s)']]
       # df['x-axis (deg/s)'] = df['x-axis (deg/s)'].apply(lambda x: x*2**16).round(0).astype(int)
       # df['y-axis (deg/s)'] = df['y-axis (deg/s)'].apply(lambda x: x*2**16).round(0).astype(int)
       # df['z-axis (deg/s)'] = df['z-axis (deg/s)'].apply(lambda x: x*2**16).round(0).astype(int)

        df2 = pd.read_csv(self._accelfile.value)
        df2 = self.tweakDataFrame(df2)
        df2 = df2[['Type','time (-00:00)','Date','epoch (ms)','x-axis (g)','y-axis (g)','z-axis (g)']]
       # df2['x-axis (g)'] = df2['x-axis (g)'].apply(lambda x: x*2**16).round(0).astype(int)
       # df2['y-axis (g)'] = df2['y-axis (g)'].apply(lambda x: x*2**16).round(0).astype(int)
       # df2['z-axis (g)'] = df2['z-axis (g)'].apply(lambda x: x*2**16).round(0).astype(int)


        df2.drop(['Type'], axis=1, inplace=True)
        df2.drop(['Date'], axis=1, inplace=True)
        df2.drop(['time (-00:00)'], axis=1, inplace=True)

        df3 = pd.read_csv(self._magfile.value)
        df3 = self.tweakDataFrame(df3)
        df3 = df3[['Type','time (-00:00)','Date','epoch (ms)','x-axis (T)','y-axis (T)','z-axis (T)']]

        df3.drop(['Type'], axis=1, inplace=True)
        df3.drop(['Date'], axis=1, inplace=True)
        df3.drop(['time (-00:00)'], axis=1, inplace=True)


        df_merged = pd.merge_asof(df.sort_values('epoch (ms)'), df2.sort_values('epoch (ms)'), on='epoch (ms)')

        df_merged = pd.merge_asof(df_merged.sort_values('epoch (ms)'), df3.sort_values('epoch (ms)'), on='epoch (ms)')

        df_merged.columns = ['Type', 'Time', 'Date', 'Timestamp', 'GyroX', 'GyroY', 'GyroZ', 'AccelX', 'AccelY', 'AccelZ', 'MagX', 'MagY', 'MagZ']

        df_merged.index.name = 'Record'

        df_merged.to_csv(outputfile)

        self.success('File combined successfully to ' + outputfile, title='Done!')


if __name__ == '__main__':

    from pyforms import start_app
    start_app(FileFixer, geometry=(200, 200, 800, 200))

 
######################################################################
#### MACHINE INFORMATION
######################################################################
 
Uname:
Linux
 
df:
Filesystem            1K-blocks       Used  Available Use% Mounted on
192.168.153.11:/home 7398118824 5351197884 1671134808  77% /home
 
quota:
 
Memory and Swap info:
MemTotal:       132037944 kB
MemFree:        128562132 kB
MemAvailable:   129024064 kB
Buffers:            3504 kB
Cached:          1805536 kB
SwapCached:            0 kB
Active:           778368 kB
Inactive:        1723384 kB
Active(anon):     721528 kB
Inactive(anon):  1172976 kB
Active(file):      56840 kB
Inactive(file):   550408 kB
Unevictable:           0 kB
Mlocked:               0 kB
SwapTotal:      125009792 kB
SwapFree:       125009792 kB
Dirty:                24 kB
Writeback:             0 kB
AnonPages:        692776 kB
Mapped:           215076 kB
Shmem:           1201800 kB
Slab:              97968 kB
SReclaimable:      47236 kB
SUnreclaim:        50732 kB
KernelStack:        4336 kB
PageTables:         8720 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:    191028764 kB
Committed_AS:    2069560 kB
VmallocTotal:   34359738367 kB
VmallocUsed:      384124 kB
VmallocChunk:   34359350492 kB
HardwareCorrupted:     0 kB
AnonHugePages:    260096 kB
HugePages_Total:       0
HugePages_Free:        0
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB
DirectMap4k:      114624 kB
DirectMap2M:    134103040 kB
 
######################################################################
#### ENVIRONMENT INFORMATION
######################################################################
 
 
LMOD_DEFAULT_MODULEPATH=/opt/ohpc/admin/modulefiles
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/.local/bin:/root/bin
MODULEPATH=/opt/ohpc/admin/modulefiles
 
######################################################################
#### DIRECTORY INFORMATION
######################################################################
 
PWD:
/home/12430898/iRELATE_DMRI_T1_PROCESSING/DMRI/tbss/original_nativeDWI_FA_tbss/mytbss_fa_subjectspecific
 
total 279516
drwxrwxr-x 4 12430898 12430898   12288 Jan 31 12:52 .
drwxrwxr-x 5 12430898 12430898    4096 Dec 19 15:54 ..
-rwxrwxr-- 1 12430898 12430898 1755785 Jul  2  2018 CON_1501_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1621735 Jul  2  2018 CON_1504_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1357994 Jul  2  2018 CON_1505_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1885189 Jul  2  2018 CON_1506_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1458960 Jul  2  2018 CON_1507_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1765406 Jul  2  2018 CON_1508_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1535111 Jul  2  2018 CON_1509_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1582151 Jul  2  2018 CON_1510_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1642841 Jul  2  2018 CON_1511_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1782096 Jul  2  2018 CON_1512_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1422671 Jul  2  2018 CON_1513_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1686528 Jul  2  2018 CON_1514_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1847886 Jul  2  2018 CON_1515_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1876856 Jul  2  2018 CON_1517_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1632495 Jul  2  2018 CON_1518_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1653666 Jul  2  2018 CON_1519_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1314054 Jul  2  2018 CON_1520_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1406034 Jul  2  2018 CON_1522_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1460511 Jul  2  2018 CON_1524_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1394292 Jul  2  2018 CON_1525_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1689473 Jul  2  2018 CON_1526_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1646688 Jul  2  2018 CON_1529_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1414354 Jul  2  2018 CON_1530_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1784960 Jul  2  2018 CON_1532_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1522894 Jul  2  2018 CON_1533_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1695715 Jul  2  2018 CON_1534_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1701033 Jul  2  2018 CON_1535_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1690734 Jul  2  2018 CON_1536_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1653408 Jul  2  2018 CON_1537_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1658482 Jul  2  2018 CON_1538_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1491650 Jul  2  2018 CON_1539_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1731706 Jul  2  2018 CON_1541_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1441648 Jul  2  2018 CON_1543_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1887108 Jul  2  2018 CON_1544_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1628812 Jul  2  2018 CON_1545_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1439041 Jul  2  2018 CON_1546_FA.nii.gz
-rwxrwxr-- 1 12430898 12430898 1690314 Jul  2  2018 CON_1547_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1548359 Jul  2  2018 CON_1549_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1638494 Jul  2  2018 CON_1550_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1582748 Jul  2  2018 CON_1553_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1733228 Jul  2  2018 CON_1554_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1530348 Jul  2  2018 CON_1556_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1661161 Jul  2  2018 CON_1560_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1679425 Jul  2  2018 CON_1563_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1838064 Oct  5 10:05 CON_1565_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1572916 Oct  5 10:05 CON_1566_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1728873 Oct  5 10:05 CON_1569_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1420717 Oct  5 10:05 CON_1571_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1529968 Oct  5 10:06 CON_1572_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1518300 Oct  5 10:06 CON_1574_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1452941 Oct  5 10:06 CON_1575_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1646163 Oct  5 10:06 CON_1576_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1603745 Oct  5 10:06 CON_1577_FA.nii.gz
-rw-rw-r-- 1 12430898 12430898 1684368 Dec 10 15:55 CON_1579_FA.nii.gz
-rw-rw-r-- 1 12430898 12430898 1596948 Dec 10 15:55 CON_1581_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1719879 Oct  5 10:06 CON_1583_FA.nii.gz
-rw-rw-r-- 1 12430898 12430898 1563009 Dec 10 15:55 CON_1584_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1736010 Jul  2  2018 CON_2501_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1925132 Jul  2  2018 CON_2502_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1721483 Jul  2  2018 CON_2504_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1842747 Jul  2  2018 CON_2505_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1885155 Jul  2  2018 CON_2506_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1523207 Jul  2  2018 CON_2508_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1822587 Jul  2  2018 CON_2509_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1761781 Jul  2  2018 CON_2510_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1466709 Jul  2  2018 CON_2513_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1762809 Jul  2  2018 CON_2515_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1513723 Jul  2  2018 CON_2516_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1516530 Jul  2  2018 CON_2517_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1597328 Jul  2  2018 CON_2519_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1982678 Jul  2  2018 CON_2521_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1541310 Jul  2  2018 CON_2522_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1648812 Jul  2  2018 CON_2524_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1584400 Jul  2  2018 CON_2528_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1820426 Jul  2  2018 CON_2529_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1759270 Jul  2  2018 CON_2530_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1569591 Jul  2  2018 CON_2531_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1589315 Jul  2  2018 CON_2532_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1612265 Jul  2  2018 CON_2533_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1390075 Jul  2  2018 CON_2536_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1755444 Jul  2  2018 CON_2537_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1744456 Jul  2  2018 CON_2540_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1592364 Jul  2  2018 CON_2542_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1642853 Jul  2  2018 CON_2544_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1595986 Jul  2  2018 CON_2546_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1562789 Jul  2  2018 CON_2547_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1248410 Jul  2  2018 CON_2548_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1622734 Jul  2  2018 CON_2550_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1875541 Jul  2  2018 CON_2551_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1480272 Jul  2  2018 CON_2552_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1576354 Jul  2  2018 CON_2553_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1878061 Jul  2  2018 CON_2554_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1597891 Jul  2  2018 CON_2556_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1962678 Jul  2  2018 CON_2557_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1760144 Jul  2  2018 CON_2559_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1521484 Jul  2  2018 CON_2560_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1595986 Jul  2  2018 CON_2562_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1652898 Jul  2  2018 CON_2563_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1756825 Jul  2  2018 CON_2566_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1897622 Jul  2  2018 CON_2567_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1389808 Jul  2  2018 CON_2569_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1698107 Jul  2  2018 CON_2571_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1695538 Jul  2  2018 CON_2572_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 2001547 Jul  2  2018 CON_2573_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1642275 Jul  2  2018 CON_2576_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1583221 Jul  2  2018 CON_2577_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1775946 Jul  2  2018 CON_2578_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1703361 Jul  2  2018 CON_2579_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1559347 Jul  2  2018 CON_2580_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1663136 Jul  2  2018 CON_2581_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1301202 Jul  2  2018 CON_2583_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1581925 Jul  2  2018 CON_2584_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1522204 Jul  2  2018 CON_2586_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1483867 Jul  2  2018 CON_2588_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1566159 Jul  2  2018 CON_2590_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1741641 Jul  2  2018 CON_2591_FA.nii.gz
-rw-rw-r-- 1 12430898 12430898 1703403 Dec 10 15:55 CON_2594_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1800120 Jul  2  2018 CON_2595_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1531112 Jul  2  2018 CON_2597_FA.nii.gz
-rw-rw-r-- 1 12430898 12430898 1496497 Dec 10 15:56 CON_2600_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1667063 Jul  2  2018 CON_2603_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1676666 Jul  2  2018 CON_2605_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1528423 Jul  2  2018 CON_2607_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1514467 Jul  2  2018 CON_2608_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1664302 Oct  5 10:06 CON_2610_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1626430 Jul  2  2018 CON_2611_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1835299 Oct  5 10:06 CON_2614_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1460835 Oct  5 10:07 CON_2618_FA.nii.gz
-rw-rw-r-- 1 12430898 12430898 1881377 Dec 10 15:56 CON_2628_FA.nii.gz
-rw-rw-r-- 1 12430898 12430898 1725499 Dec 10 15:56 CON_2633_FA.nii.gz
drwxrwxr-x 3 12430898 12430898 7208960 Jan 31 13:20 FA
-rwxr-xr-- 1 12430898 12430898 1551773 Jul  2  2018 PAT_1101_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1813880 Jul  2  2018 PAT_1103_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1875399 Jul  2  2018 PAT_1106_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1615953 Jul  2  2018 PAT_1109_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1558177 Jul  2  2018 PAT_1116_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1567750 Jul  2  2018 PAT_1117_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1884778 Jul  2  2018 PAT_1118_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1832545 Jul  2  2018 PAT_1119_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1532455 Jul  2  2018 PAT_1127_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1659802 Oct  5 10:07 PAT_1142_FA.nii.gz
-rw-rw-r-- 1 12430898 12430898 1681014 Dec 10 15:56 PAT_1145_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1558893 Jul  2  2018 PAT_2101_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1501982 Jul  2  2018 PAT_2103_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1608727 Jul  2  2018 PAT_2106_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1271431 Jul  2  2018 PAT_2110_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1689720 Jul  2  2018 PAT_2111_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1515271 Jul  2  2018 PAT_2112_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1733697 Jul  2  2018 PAT_2115_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1736455 Jul  2  2018 PAT_2116_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1678947 Jul  2  2018 PAT_2117_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1340343 Jul  2  2018 PAT_2118_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1578350 Jul  2  2018 PAT_2119_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1753780 Jul  2  2018 PAT_2120_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1658410 Jul  2  2018 PAT_2128_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1555222 Jul  2  2018 PAT_2129_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1524605 Jul  2  2018 PAT_2130_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1769232 Jul  2  2018 PAT_2137_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1582748 Jul  2  2018 PAT_2138_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1553166 Jul  2  2018 PAT_2140_FA.nii.gz
-rwxr-xr-- 1 12430898 12430898 1956532 Jul  2  2018 PAT_2143_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1786372 Oct  5 10:07 PAT_2151_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1383637 Oct  5 10:07 PAT_2156_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1241523 Oct  5 10:07 PAT_2157_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1545343 Oct  5 10:07 PAT_2158_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1826403 Oct  5 10:07 PAT_2159_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1574430 Oct  5 10:07 PAT_2160_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1370622 Oct  5 10:08 PAT_2161_FA.nii.gz
-rw-r--r-- 1 12430898 12430898 1786281 Oct  5 10:08 PAT_2162_FA.nii.gz
-rw-rw-r-- 1 12430898 12430898 1456151 Dec 10 15:56 PAT_2164_FA.nii.gz
-rw-rw-r-- 1 12430898 12430898 1732389 Dec 10 15:56 PAT_2166_FA.nii.gz
drwxrwxr-x 2 12430898 12430898    4096 Jan 31 13:43 stats
-rwxrwxr-- 1 12430898 12430898   77604 Jan 31 12:39 .tbsslog
 
######################################################################
#### IMAGE INFORMATION
######################################################################
 
